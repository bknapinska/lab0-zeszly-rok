﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Meble);
        public static Type B = typeof(Krzeslo);
        public static Type C = typeof(Stol);

        public static string commonMethodName = "ToString";
    }
}
