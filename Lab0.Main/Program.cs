﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Meble
    {
        private int wielkosc;
        private int cena;
        public Meble()
        {
            wielkosc = 10;
            cena = 50;
        }
        public Meble(int a, int b)
        {
            wielkosc = a;
            cena = b;
        }
        public override string ToString()
        {
            return "Dobry";
        }
    }
    class Krzeslo : Meble
    {
        private string material;
        public Krzeslo() : base(15, 30) { this.material = "Kashmir"; }
        public override string ToString()
        {
            return "Zły";
        }
  
    }
    class Stol : Meble
    {
        private int iloscnog;
        public Stol() :base(30,123)
        {
            this.iloscnog = 3;
        }
        public override string ToString()
        {
            return "Przeciętny";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Krzeslo k = new Krzeslo();
            Stol s = new Stol();
            Meble m = new Meble();
            Console.WriteLine(k.ToString());
            Console.WriteLine(s.ToString());
            Console.WriteLine(m.ToString());
            Console.ReadKey();
        }
    }
}
